import java.util.ArrayList;

import com.devcamp.j03_javabasic.s10.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        //Subtask 2: Khởi tạo 4 object “Person” với 4 loại constructor trên và cho vào một ArrayList (làm tại hàm main)
        ArrayList<Person> arrayList = new ArrayList<>();
        //Khởi tạo Person với các tham số khác nhau
        Person person0 = new Person();
        Person person1 = new Person("Devcamp");
        Person person2 = new Person("Viet", 24, 66.7);
        Person person3 = new Person("Nam", 25, 68.8, 50000000, new String[]{"Little Camel"});
    
        //thêm object Person vào danh sách
        arrayList.add(person0);
        arrayList.add(person1);
        arrayList.add(person2);
        arrayList.add(person3);

        //in ra màn hình
        for(Person person : arrayList) {
            //System.out.println(person);
            System.out.println(person.toString());  
        }

    }
}
